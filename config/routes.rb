Rails.application.routes.draw do
  devise_for :admins

  devise_scope :admin do 
    get "admins/users/list", to: "admins/sessions#list", as:"users_list"
  end

  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  resources :zombies do
    resources :brains
  end
  get '/sexto/cerebros', to: 'brains#index', as: 'brains'
  root to: 'zombies#index'
end
